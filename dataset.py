"""
Class for storing and managing the data used in the transformation software.
:author: Adam Glover
"""

# External Libraries
try:
    from tabulate import tabulate
except:
    print("Error: Tabulate isn't installed.")
    print("Install tabulate via 'pip install tabulate'.")
try:
    import xmltodict
except:
    print("Error: XMLtodict isn't installed.")
    print("Install XMLtodict via 'pip install xmltodict'.")

# Built-in libraries
import json
import csv
import os
from zipfile import ZipFile
from collections import OrderedDict
import re

# Utility
from utility import postprocessor


class DataSet:
    READ_TYPES = (".csv", ".json", ".xml")
    WRITE_TYPES = (".json", ".csv", ".xml", ".html", ".tex", ".md")
    FLATTEN_TYPES = (".csv", ".html", ".tex", ".md")

    def __init__(self):
        self.path = ""
        self.path_out = ""
        self.extension = ""
        self.has_header = True
        self.headers = []
        self.data = []  # Json-like
        self.data_flattened = []

    def read_file(self, path):
        """
        Reads the data from the file at path.
        :param path: The path of the file to read.
        :return: True if successfully read. False if it fails.
        """
        self.path = os.path.abspath(path)
        self.extension = os.path.splitext(self.path)[1]

        # print("Reading: %s" % self.path)

        out = False
        if self.extension in self.READ_TYPES:
            if self.extension == ".csv":
                out = self._load_csv()
            elif self.extension == ".json":
                out = self._load_json()
            elif self.extension == '.xml':
                out = self._load_xml()
            else:
                out = False
        else:
            out = False

        # If data was successfully read, remove spaces from the keys, and obtain the headers in self.data.
        if out:
            self.data = self._remove_specials(self.data)
            self.headers = self._obtain_headers(self.data)
            #print(self.headers)
        return out

    def write_file(self, file_types):
        """
        Writes the data contained within the internal data format to the provided file types.
        :param file_types: A tuple, string, or list of filetypes to write the data to.
        :return: The path of a zip file containing all the file types. None if one of the writes is unsuccessful.
        """

        # Converts a string input into a tuple
        if type(file_types) == str:
            file_types = tuple(file_types)

        # Defines output path and creates folder.
        self.path_out = os.path.join(os.path.dirname(self.path), "%s-outputs" % os.path.split(self.path)[-1].replace(".", "-"))
        if not os.path.exists(self.path_out):
            os.mkdir(self.path_out)

        # Flattens data if a chosen file_type requires it.
        if any(i in self.FLATTEN_TYPES for i in file_types):
            self.data_flattened = self._flatten_data()

        out = True
        # Iterates through file_types and calls the relevant write methods.
        if type(file_types) == list or type(file_types) == tuple:
            for file_type in file_types:
                if file_type in self.WRITE_TYPES:
                    if file_type == '.json':
                        if not self._write_json():
                            out = False
                    elif file_type == '.csv':
                        if not self._write_csv():
                            out = False
                    elif file_type == '.xml':
                        if not self._write_xml():
                            out = False
                    elif file_type == '.html':
                        if not self._write_html():
                            out = False
                    elif file_type == ".tex":
                        if not self._write_tex():
                            out = False
                    elif file_type == ".md":
                        if not self._write_md():
                            out = False
        else:
            return None

        # Returns None if one of the writes was unsuccessful.
        if not out:
            return None

        # Creates a zip of all the outputs, returns the file location
        path_zip = os.path.join(os.path.dirname(self.path), "%s-outputs.zip" % os.path.split(self.path)[-1].replace(".", "-"))
        with ZipFile(path_zip, 'w') as file:
            for folder_name, sub_folders, file_names in os.walk(self.path_out):
                for file_name in file_names:
                    file_path = os.path.join(folder_name, file_name)
                    file.write(file_path, os.path.basename(file_path))
        return path_zip

    """
    File loading
    """

    def _load_csv(self):
        """
        Reads the data contained within a csv (or tsv) file.
        :return: True if successfully read. False if it fails.
        """
        try:
            # Reads the file
            with open(self.path, mode='r') as file:
                data = file.read()

            # Parses all the data in the file
            sniffer = csv.Sniffer()
            dialect = sniffer.sniff(data)
            self.has_header = sniffer.has_header(data)
            data_list = data.splitlines()
            try:
                data_list.remove("")  # Removes blank lines
            except ValueError:
                pass
            csv_data = list(csv.reader(data_list, skipinitialspace=True, dialect=dialect, quoting=csv.QUOTE_NONNUMERIC))
            num_fields = len(max(csv_data, key=len))

            # Handles and sorts out the header
            if self.has_header:
                self.headers = csv_data[0]
                csv_data.pop(0)
            if len(self.headers) < num_fields:
                for i in range(num_fields - len(self.headers)):
                    self.headers.append("FIELD%s" % (i+1))

            # Adds to the internal representation
            for entry in csv_data:
                curr_item = {}
                for i in range(num_fields):
                    if i > len(entry)-1:
                        curr_item[self.headers[i]] = None
                    else:
                        if entry[i] == '':
                            curr_item[self.headers[i]] = None
                        else:
                            curr_item[self.headers[i]] = entry[i]
                self.data.append(curr_item)

            # print("Successfully Read: %s" % self.path)
            return True
        except:
            return False

    def _load_json(self):
        """
        Reads data from a .json document.
        :return: True if successfully read. False if it fails.
        """
        try:
            with open(self.path, mode='r') as file:
                data = json.load(file)

            if type(data) == list:
                self.data = data
            else:
                self.data.append(data)
            return True
        except:
            return False

    def _load_xml(self):
        """
        Reads data from an .xml document.
        :return: True if successfully read. False if it fails.
        """
        try:
            with open(self.path, encoding='utf-8') as file:
                read_string = file.read()
                data = xmltodict.parse(read_string, postprocessor=postprocessor, encoding='utf-8')
            if len(data) == 1:
                self.data.append(data[list(data.keys())[0]])
            else:
                self.data.append(data)
            return True
        except:
            return False

    """
    File writing
    """

    def _write_csv(self):
        """
        Writes data to a .csv document.
        :return: True if successfully read. False if it fails.
        """
        try:
            # Flattens self.data
            to_write = self.data_flattened
            # Writes out to output.csv
            path = os.path.join(self.path_out, "output.csv")
            with open(path, "w", newline="") as file:
                writer = csv.writer(file, quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
                if self.has_header:
                    writer.writerows(to_write)
                else:
                    writer.writerows(to_write[1:])
            return True
        except:
            return False

    def _write_json(self):
        """
        Writes data to a .json document.
        :return: True if successfully read. False if it fails.
        """
        try:
            # Writes out to output.json
            path = os.path.join(self.path_out, "output.json")
            with open(path, "w") as file:
                if len(self.data) > 1:
                    json.dump(self.data, file, indent=2)
                else:
                    json.dump(self.data[0], file, indent=2)
            return True
        except:
            return False

    def _write_xml(self):
        """
        Writes data to an .xml document.
        :return: True if successfully read. False if it fails.
        """
        try:
            # Data preparation
            if len(self.data) == 1:
                to_unparse = {"root": self.data[0]}
            else:
                to_unparse = {"root": {"row": self.data}}
            # Convert to XML
            to_write = xmltodict.unparse(to_unparse, pretty=True)
            to_write = re.sub(r'<(/?)(\d[^>]*)>', r'<\1_\2>', to_write)
            # Writes out to output.xml
            path = os.path.join(self.path_out, "output.xml")
            with open(path, "w") as file:
                file.write(to_write)
            return True
        except:
            return False

    def _write_html(self):
        """
        Writes data to a html document.
        :return: True if successfully read. False if it fails.
        """
        try:
            # Basic styling.
            to_write = "<style> table, th, td { border: 1px solid black; border-collapse: collapse; } </style>\n"
            # Calls tabulate to convert the flattened data to HTML.
            if self.has_header:
                to_write += tabulate(self.data_flattened, headers="firstrow", tablefmt="html")
            else:
                to_write += tabulate(self.data_flattened, tablefmt="html")

            # Writes out to output.html
            path = os.path.join(self.path_out, "output.html")
            with open(path, "w") as file:
                file.write(to_write)
            return True
        except:
            return False

    def _write_tex(self):
        """
        Writes data to a LaTeX document.
        :return: True if successfully read. False if it fails.
        """
        try:
            # Calls tabulate to convert the flattened data to LaTeX.
            # Automatically converts special characters to their LaTeX variants.
            if self.has_header:
                to_write = tabulate(self.data_flattened, headers="firstrow", tablefmt="latex")
            else:
                to_write = tabulate(self.data_flattened, tablefmt="latex")

            # Writes out to output.html
            path = os.path.join(self.path_out, "output.tex")
            with open(path, "w") as file:
                file.write(to_write)
            return True
        except:
            return False

    def _write_md(self):
        """
        Writes data to a Markdown document.
        :return: True if successfully read. False if it fails.
        """
        try:
            # Calls tabulate to convert the flattened data to Markdown.
            if self.has_header:
                to_write = tabulate(self.data_flattened, headers="firstrow", tablefmt="github")
            else:
                to_write = tabulate(self.data_flattened, tablefmt="github")

            # Writes out to output.html
            path = os.path.join(self.path_out, "output.md")
            with open(path, "w") as file:
                file.write(to_write)
            return True
        except:
            return False

    """
    Utility
    """

    def _remove_specials(self, d):
        """
        Within the keys, replaces spaces with '_' and removes non alpha-numeric characters.
        This is useful as some data types (such as XML) do not accept non alpha-numeric values within their keys/tags.
        :return: Base case: new self.data with alpha-numeric characters removed.
        """
        data = d
        if isinstance(data, list):
            new_data = []
            for item in data:
                new_data.append(self._remove_specials(item))
            data = new_data
        elif isinstance(data, (dict, OrderedDict)):
            new_data = {}
            keys = list(data.keys())
            for key in keys:
                new_key = key.replace(" ", "_")
                new_key = re.sub(r'\W+', '', new_key)
                new_data[new_key] = self._remove_specials(data[key])
            data = new_data
        return data

    def _obtain_headers(self, data):
        """
        Recursively obtains all the keys from self.data
        Used for defining a field to perform transformations to.
        :return: Base case: All the keys in data (self.data)
        """
        key_list = []
        if isinstance(data, list):
            for item in data:
                child_keys = self._obtain_headers(item)
                key_list = key_list + list(set(child_keys) - set(key_list))  # Adds new keys to key_list
        elif isinstance(data, (dict, OrderedDict)):
            keys = list(data.keys())
            key_list.extend(keys)
            for key in keys:
                child_keys = self._obtain_headers(data[key])
                key_list = key_list + list(set(child_keys) - set(key_list))  # Adds new keys to key_list
        return key_list

    def _flatten_data(self):
        """
        Flattens self.data into a 2d array. Used for outputting to CSV, HTML, LaTeX, etc.
        Nested function (flatten) originally written by Amir Ziai, 2015. Modified for the purpose of the program.
        https://towardsdatascience.com/flattening-json-objects-in-python-f5343c794b10
        :return: Self.data flattened into a list of lists.
        """
        # Nested function that flattens json into a single dictionary.
        def flatten(x, name=''):
            if isinstance(x, (dict, OrderedDict)):
                for a in x:
                    flatten(x[a], name + a + '_')
            elif isinstance(x, list):
                i = 0
                for a in x:
                    flatten(a, name + str(i) + '_')
                    i += 1
            else:
                out[name[:-1]] = x

        # Outputs an array of dicts

        # Traverses the data set until it finds an array or dict with length > 1.
        # Some data may be lost, but it allows the data to be readable.
        new_data = self.data
        while len(new_data) == 1:
            if type(new_data) == list:
                new_data = new_data[0]
                continue
            elif type(new_data) in (dict, OrderedDict):
                new_data = new_data[list(new_data.keys())[0]]
                continue
            else:
                break
        if type(new_data) != list:
            new_data = [new_data]

        # Loop that calls the flattener.
        data_flat_dict = []
        keys = []
        for item in new_data:
            out = {}
            flatten(item)
            data_flat_dict.append(out)
            # Eventually 'keys' will contain all the keys in the dataset.
            for key in out:
                if key not in keys:
                    keys.append(key)

        # Converts array of dicts into an array of arrays. First array is the headers.
        # Creates rows
        data_flat = []
        number_keys = len(keys)
        for item in data_flat_dict:
            row = [None] * number_keys
            for i in range(number_keys):
                if keys[i] in item.keys():
                    row[i] = item[keys[i]]
            data_flat.append(row)

        # Insert keys into the top of the 2d array
        data_flat.insert(0, keys)
        return data_flat


# Tester
if __name__ == "__main__":
    from transformer import Transformer
    # Testing with no transformations
    inputs = ["./example_files/example1.csv", "./example_files/example2.json", "./example_files/example3.csv",
              "./example_files/example4.xml", "./example_files/example5.csv", "./example_files/example6.json",
              "./example_files/example7.xml"]
    outputs = list(DataSet.WRITE_TYPES)
    for ins in inputs:
        x = DataSet()
        print("Reading File: %s" % ins)
        if x.read_file(ins):
            print("Successfully Read.")
            print("Writing to files: %s" % outputs)
            t = Transformer()
            t.set_transformations(['string-upper "ALL"', 'math-add "ALL" 500000'])
            x.data = t.do_transform(x)
            if x.write_file(outputs):
                print("Successfully Wrote.")
            else:
                print("There was an error with one of the writes.")
        else:
            print("Failed")
        print()
