"""
Manages the functionality of the Flask program. Contains all the views.
:author: Adam Glover
"""

# External Libraries
try:
    from flask import Flask, render_template, request, session, redirect, flash, url_for, send_file
except:
    print("Error: Flask isn't installed.")
    print("Install Flask via 'pip install Flask'.")

try:
    from apscheduler.schedulers.background import BackgroundScheduler
except:
    print("Error: APSheduler isn't installed.")
    print("Install APSheduler via 'pip install APScheduler'.")

try:
    from werkzeug.utils import secure_filename
except:
    print("Error: Werkzeug isn't installed.")
    print("Install Werkzeug via 'pip install Werkzeug'.")

# Built-in libraries
import os
import shutil
import sys
import uuid
import pickle
import json
import atexit
import time

# Packages
from dataset import DataSet
from transformer import Transformer


# Create instance of the application and load the config.
app = Flask(__name__)
app.config.from_object('config')


# Homepage. Allows the user to upload file.
@app.route('/', methods=['GET', 'POST'])
def upload_file():
    session['data'] = None
    session['download'] = None
    if request.method == 'POST':
        # Validates field of form
        if 'file' not in request.files:
            flash('Error: File field in form is missing.')
            return redirect(request.url)

        # Checks if a file was uploaded.
        file = request.files['file']
        if file.filename == '':
            flash('Error: No file uploaded.')
            return redirect(request.url)

        if file:
            # Obtain name and extension of uploaded file.
            file_name = secure_filename(file.filename)
            file_ext = os.path.splitext(file_name)[1]

            # Checks if it's a type that is readable by DataSet.
            if file_ext not in DataSet.READ_TYPES:
                flash("Error: Uploaded file-type '%s' is not currently supported." % file_ext)
                return redirect(request.url)

            # Define the folder and file paths.
            folder_path = os.path.join(app.config['UPLOAD_FOLDER'], str(uuid.uuid4()))
            file_path = os.path.join(folder_path, file_name)
            os.mkdir(folder_path)  # Creates folder
            file.save(file_path)   # Saves file to folder

            # Creates instance of DataSet and reads the file.
            ds = DataSet()
            success = ds.read_file(file_path)
            if success:
                session['data'] = pickle.dumps(ds)  # Serialises DataSet and stores it as a session.
                return redirect(url_for('transform_file'))
            else:
                flash("Error: There was a problem reading the file. Please make sure its syntax is correct.")
                shutil.rmtree(folder_path)  # Deletes uploaded file
                return redirect(request.url)

    # Render template
    return render_template('upload.html', types=DataSet.READ_TYPES)


# Transformation page
@app.route('/transform', methods=['GET', 'POST'])
def transform_file():
    # Validation
    # Ensures that a file has been uploaded.
    if not session['data']:
        flash("Error: Please upload a file first.")
        return redirect(url_for('upload_file'))

    # Obtains data from session
    d = pickle.loads(session['data'])

    # Makes sure the data hasn't been deleted.
    if not os.path.isfile(d.path):
        flash("Error: Session has expired. Please re-upload your file.")
        session['data'] = None
        return redirect(url_for('upload_file'))

    # Get Method
    if request.method == 'GET':
        # Pretty representation of d.data
        preview = json.dumps(d.data, indent=2)

        # Orders the headers alphabetically
        headers = d.headers
        headers.sort()

        # Render template
        return render_template('transform.html',
                               fields=['ALL']+headers,
                               transforms=Transformer.METHODS,
                               preview=preview,
                               write_types=DataSet.WRITE_TYPES)
    # Post Method
    if request.method == 'POST':
        # Obtains form data.
        form = request.form
        fields = list(form.keys())

        # Checks conversion outputs.
        write_types = []
        for field in fields:
            if field in DataSet.WRITE_TYPES:
                write_types.append(field)
        if not len(write_types):
            flash("Error: Please select an output type")
            return redirect(request.url)

        # Does transformations
        if 'transform-area' in fields:
            # Does transformations to d.
            transform_list = form['transform-area'].split(os.linesep)
            if len(transform_list):
                t = Transformer()
                t.set_transformations(transform_list)
                d.data = t.do_transform(d)

        # Writes out to the output files.
        zip_path = d.write_file(write_types)
        if not zip_path:
            flash("Error: There was an unknown error writing to file.")
            return redirect(request.url)
        else:
            session['download'] = zip_path
            return redirect(url_for("download"))
    # Other methods
    return "Invalid method"


# Final download page
@app.route('/download', methods=['GET', 'POST'])
def download():
    if request.method == 'POST':
        if not session['download']:
            flash("Error: You have no current session to download")
            return redirect(request.url)
        if not os.path.isfile(session['download']):
            flash("Error: File is no longer present")
            session['download'] = None
            return redirect(request.url)
        return send_file(session['download'])
    return render_template('download.html')


# AJAX Method, used in the transformation page for updating the preview.
@app.route('/update', methods=['GET', 'POST'])
def update():
    if request.method == 'POST':
        # Ensuring there is a valid session.
        if not session['data']:
            return "Error: Session not found"

        # Obtains data from session.
        d = pickle.loads(session['data'])

        # Makes sure the data hasn't been deleted.
        if not os.path.isfile(d.path):
            session['data'] = None
            return "Error: Session has expired. Please re-upload your file."

        # Proceed to processing the query.
        data = json.loads(request.data.decode('utf-8'))
        transforms = data.get('transforms')

        # Do transformations if transformations have been specified.
        if len(transforms):
            t = Transformer()
            t.set_transformations(transforms)
            transform_data = t.do_transform(d)
            return str(json.dumps(transform_data, indent=2))
        else:
            return str(json.dumps(d.data, indent=2))
    return "Error: Invalid query"


def cleanup_files():
    """
    Job for APScheduler's Background Scheduler.
    Removes files and folders within the upload folder that are older than CLEANUP_TIME
    """
    folder = app.config['UPLOAD_FOLDER']
    cleanup_time = app.config['CLEANUP_TIME']

    # Obtains all the current folders
    sub_folders = next(os.walk(folder))[1]
    folder_list = []
    for sub_folder in sub_folders:
        folder_list.append(os.path.join(folder, sub_folder))

    # Deletes folders older than cleanup_time
    current_time = time.time()
    for folder in folder_list:
        creation_time = os.stat(folder).st_ctime
        if (current_time - creation_time) > cleanup_time:
            print("Deleted folder: %s" % folder)
            shutil.rmtree(folder)


# Run
if __name__ == '__main__':
    DEBUG = False

    # Sets port to command line argument - "4231" if unspecified.
    PORT = "4231"
    if len(sys.argv) > 1:
        if sys.argv[1].isdigit():
            PORT = sys.argv[1]

    if not DEBUG:
        if os.path.isdir(app.config['UPLOAD_FOLDER']):
            shutil.rmtree(app.config['UPLOAD_FOLDER'])  # Deletes uploaded files folder.
        os.mkdir(app.config['UPLOAD_FOLDER'])           # Creates blank uploaded files folder.

        # Creates and starts scheduler that periodically deletes old folders.
        scheduler = BackgroundScheduler()
        scheduler.add_job(func=cleanup_files, trigger="interval", seconds=60)
        scheduler.start()
        atexit.register(lambda: scheduler.shutdown())

    app.run(debug=DEBUG, port=PORT)                 # Starts the application.
