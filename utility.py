"""
Utility functions used throughout the software.
:author: Adam Glover
"""


def postprocessor(path, key, value):
    """
    For XMLtodict.
    As all values are stored as strings, it converts them into integers/floats if possible.
    Also removes any leading underscore
    """
    try:
        if key[0] == '_':
            key = key[1:]
        return key, int(value)
    except:
        try:
            return key, float(value)
        except:
            return key, value
