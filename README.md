# User Manual

Demonstation video playlist: https://www.youtube.com/playlist?list=PLIbcixtBjNlEA9TWs5KeeoMoNcDHhlAkj

## Windows Installation & Execution

<details><summary>(Click to Expand)</summary>


**You must have python3 installed for this to work. While most versions of Python3 are likely to work, this project was developed on Python 3.7.4 so if any errors occur try specifically using this version.**

### Install

* Open up a command line prompt in the desired directory and run `git clone https://gitlab.com/aglover1/file-transformer.git`
    * Alternatively, download the repository as a zip and extract.
* Then, from the base directory of the project, open up a command line prompt and enter `pip install --user -r requirements.txt`

### Run

* With a command line prompt open in the base of the directory, enter `python main.py`
* Open up a web browser and enter `127.0.0.1:4231` in the address bar.
    * If this gives an unexpected result. Instead run `python main.py PORT`, replacing PORT with a number of your choosing. The website you must then access is located at **127.0.0.1:PORT**, again with PORT replaced by the chosen number.

</details>

## Feng-Linux Installation & Execution

<details><summary>(Click to Expand)</summary>

### Install

* Open up a command line prompt in the desired directory and run `git clone https://gitlab.com/aglover1/file-transformer.git`
    * Alternatively, download the repository as a zip and extract.
* Enter the base directory of the project and open up a command line prompt, enter the following five commands commands:
    * `module add anaconda3`
    * `python3 -m venv convtool`
    * `source convtool/bin/activate`
    * `convtool/bin/pip install -r requirements.txt`
    * `deactivate`

### Run

* Open up a command line prompt within the base of the directory and enter the following commands:
    * `source convtool/bin/activate`
    * `python3 main.py`
* Open up a web browser and enter `127.0.0.1:4231` in the address bar.

</details>

## How to Use

<details><summary>(Click to Expand)</summary>

* On the homepage, upload a file and click 'upload'.

* If it's a filetype currently accepted by the program, and it's successfully read, it will redirect you to the transformation page.

* If you wish to transform the data, at the top of the page select the transformation you wish to perform, along with the target field (Or ALL fields), and any extra values that the transformation requires. Then, click add.

* The currently accepted transformations are:

    * **math-multiply val**: Multiplies the specified field by 'val'.

    * **math-divide val**: Divides the specified field by 'val'.

    * **math-add val**: Adds 'val' to the specified field.

    * **math-minus val**: Minuses 'val' from the specified field.

    * **string-upper**: Converts the strings belonging to the specified field to upper-case.

    * **string-lower**: Converts the strings belonging to the specified field to lower-case.

    * **string-capitalize**: Capitalizes the first letter of the strings belonging to the specified field.

    * **math-round val**: Rounds float values within field to val decimal places.

    * More transformations can be added to the program by modifying the code due to it's expandability.

* If you wish to preview the data with the transformations applied, click the **update** button above the right-hand box.

* Once the data looks how you want, select the output types at the bottom and click 'convert'. You should then be redirected to the download page.

* Click 'download' to download a zip file of the converted data.

* To stop the program, close the browser window and press 'CTRL+C' on your keyboard whilst in the command prompt that is running the program.

</details>
