"""
Class for transforming class DataSet in DataSet.py
:author: Adam Glover
"""

# External Libraries
try:
    import xmltodict
except:
    print("Error: XMLtodict isn't installed.")
    print("Install XMLtodict via 'pip install xmltodict'.")

# Built-in libraries
import shlex
from xml.etree import cElementTree as ElementTree
import re

# Utility
from utility import postprocessor


class Transformer:
    """
    Transformations are defined in the form '<method> "<field>" <args>".
    """
    METHODS = ("math-multiply", "math-divide", "math-add", "math-minus", "string-upper", "string-lower",
               "string-capitalize", "math-round")

    def __init__(self):
        self.transforms = []

    def set_transformations(self, t_array):
        """
        Stores t inside the internal transformation array.
        :param t_array: Array of transformations
        :return: None
        """
        self.transforms = t_array

    def do_transform(self, ds):
        """
        Applies the transformations to d. Converts it to XML in order to do so.
        :param ds: Dataset.data.
        :return: A version of DataSet.data with the transformations applied
        """
        # Obtains the headers and converts the data into XML.
        fields = ds.headers
        data = self._to_xml(ds.data)

        for transform in self.transforms:
            try:
                if len(transform) == 0:
                    continue

                # Obtains the details of the current transformation
                t_split = shlex.split(transform)
                method = t_split[0]
                field = t_split[1]
                args = t_split[2:]

                # Checks that it's being applied to a valid field
                if field not in fields and field != "ALL":
                    continue

                if field[0].isdigit():
                    field = "_"+field

                # Calls the relevant function
                if method == "math-multiply":
                    self._do_math_multiply(data, field, args[0])
                elif method == "math-divide":
                    self._do_math_divide(data, field, args[0])
                elif method == "math-add":
                    self._do_math_add(data, field, args[0])
                elif method == "math-minus":
                    self._do_math_minus(data, field, args[0])
                elif method == "string-upper":
                    self._do_string_capitalize(data, field, 'u')
                elif method == "string-lower":
                    self._do_string_capitalize(data, field, 'l')
                elif method == "string-capitalize":
                    self._do_string_capitalize(data, field, 'c')
                elif method == "math-round":
                    self._do_math_round(data, field, args[0])
            except:
                continue

        # Converts the XML back to Json, and returns it.
        return self._to_data(data)

    """
    Converting to and from the transformation format.
    """

    def _to_xml(self, d):
        """
        Converts DataSet.data into an XML tree.
        :param d: DataSet.data
        :return: XML tree of DataSet.data
        """
        #print(json.dumps(d, indent=2))
        if len(d) == 1:
            to_unparse = {"ALL": d[0]}
        else:
            to_unparse = {"ALL": {"row": d}}
        # Convert to XML
        to_tree = xmltodict.unparse(to_unparse, pretty=True, encoding='utf-8')
        to_tree = re.sub(r'<(/?)(\d[^>]*)>', r'<\1_\2>', to_tree)  # Adds _ to start of tag if it begins with an integer
        data = ElementTree.fromstring(to_tree)
        return data

    def _to_data(self, d):
        """
        Converts the transformed XML tree back into a form accepted by DataSet.data
        :param d: XML tree
        :return: DataSet.data representation
        """
        x = xmltodict.parse(ElementTree.tostring(d, encoding='utf8', method='xml'), postprocessor=postprocessor, encoding='utf-8')

        if "row" in list(x['ALL'].keys()) and len(x['ALL']) == 1:
            data = x['ALL']['row']
        else:
            data = x['ALL']

        if not isinstance(data,list):
            data = [data]
        # print(json.dumps(data, indent=2))
        return data

    """
    Utility methods
    """

    def _get_value(self, v):
        """
        Converts a string of a number into an integer or string.
        :param v: String version of a float/integer
        :return: Float/integer of the string
        """
        try:
            value = int(v)
        except:
            try:
                value = float(v)
            except:
                value = v
        return value

    """
    Transformation methods
    """

    def _do_math_multiply(self, data, field, value):
        """
        Multiplies 'value' to all the number data within 'data' below 'field'.
        :param data: XML tree of the data (pointer)
        :param field: Field to do the operation on
        :param value: Value to multiply by
        :return: None
        """
        value = self._get_value(value)
        if not isinstance(value, (int, float)):
            return

        # Obtains all the data that needs to be transformed.
        if field == "ALL":
            items = data.findall(".//*")
        else:
            items = data.findall(".//%s" % field)
            items.extend(data.findall(".//%s/*" % field))

        # Iterates through the data and applies the transformation.
        for item in items:
            try:
                num = float(item.text)
                if num.is_integer():
                    num = int(num)
                item.text = str(num * value)
            except:
                pass

    def _do_math_divide(self, data, field, value):
        """
        Divides 'value' to all the number data within 'data' below 'field'.
        :param data: XML tree of the data (pointer)
        :param field: Field to do the operation on
        :param value: Value to divide by
        :return: None
        """
        value = self._get_value(value)
        if not isinstance(value, (int, float)):
            return

        # Obtains all the data that needs to be transformed.
        if field == "ALL":
            items = data.findall(".//*")
        else:
            items = data.findall(".//%s" % field)
            items.extend(data.findall(".//%s/*" % field))

        # Iterates through the data and applies the transformation.
        for item in items:
            try:
                num = float(item.text)
                if num.is_integer():
                    num = int(num)
                item.text = str(num / value)
            except:
                pass

    def _do_math_add(self, data, field, value):
        """
        Adds 'value' to all the number data within 'data' below 'field'.
        :param data: XML tree of the data (pointer)
        :param field: Field to do the operation on
        :param value: Value to add by
        :return: None
        """
        value = self._get_value(value)
        if not isinstance(value, (int, float)):
            return

        # Obtains all the data that needs to be transformed.
        if field == "ALL":
            items = data.findall(".//*")
        else:
            items = data.findall(".//%s" % field)
            items.extend(data.findall(".//%s/*" % field))

        # Iterates through the data and applies the transformation.
        for item in items:
            try:
                num = float(item.text)
                if num.is_integer():
                    num = int(num)
                item.text = str(num + value)
            except:
                pass

    def _do_math_minus(self, data, field, value):
        """
        Minuses 'value' from all the number data within 'data' below 'field'.
        :param data: XML tree of the data (pointer)
        :param field: Field to do the operation on
        :param value: Value to subtract by
        :return: None
        """
        value = self._get_value(value)
        if not isinstance(value, (int, float)):
            return

        # Obtains all the data that needs to be transformed.
        if field == "ALL":
            items = data.findall(".//*")
        else:
            items = data.findall(".//%s" % field)
            items.extend(data.findall(".//%s/*" % field))

        # Iterates through the data and applies the transformation.
        for item in items:
            try:
                num = float(item.text)
                if num.is_integer():
                    num = int(num)
                item.text = str(num - value)
            except:
                pass

    def _do_math_round(self, data, field, value):
        """
        Rounds all the floats below 'field' to 'value' decimal places.
        :param data: XML tree of the data (pointer)
        :param field: Field to do the operation on
        :param value: Value decimal places to round to
        :return None
        """
        value = self._get_value(value)
        if not isinstance(value, int):
            return

        # Obtains all the data that needs to be transformed.
        if field == "ALL":
            items = data.findall(".//*")
        else:
            items = data.findall(".//%s" % field)
            items.extend(data.findall(".//%s/*" % field))

        # Iterates through the data and applies the transformation.
        for item in items:
            try:
                num = float(item.text)
                if num.is_integer():
                    num = int(num)
                item.text = str(round(num, value))
            except:
                pass


    def _do_string_capitalize(self, data, field, mode):
        """
        Alters the case of strings within data that belong to 'field'.
        :param data: XML tree of the data (pointer)
        :param field: Field to do the transformation on
        :param mode: 'u' = uppercase, 'l' = lowercase, 'c' = capitalise first letter.
        :return: None
        """
        # Obtains all the data that needs to be transformed.
        if field == "ALL":
            items = data.findall(".//*")
        else:
            items = data.findall(".//%s" % field)
            items.extend(data.findall(".//%s/*" % field))

        # Iterates through the data and applies the transformation.
        for item in items:
            if mode == 'u':
                item.text = item.text.upper()
            elif mode == 'l':
                item.text = item.text.lower()
            elif mode == 'c':
                item.text = item.text.capitalize()
