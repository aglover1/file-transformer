"""
Configuration file for the Flask program.
:author: Adam Glover
"""

# Built-in libraries
import os

basedir = os.path.abspath(os.path.dirname(__file__))
SECRET_KEY = '7x42b54Ap$%UtpHxiDbnbKrK&cJM95E5xz^zE%4Ht@S#$exvcV'
UPLOAD_FOLDER = os.path.join(basedir, 'uploaded_files')
MAX_CONTENT_LENGTH = 16 * 1024 * 1024  # Maximum 16mb upload.
CLEANUP_TIME = 1800  # Seconds since creation before a folder is deleted. (30 minutes)
