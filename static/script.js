function add() {
    // Adds specified transformation to the transformation box.

    var method = document.getElementById("transforms").value
    var field  =  document.getElementById("fields").value
    var values = document.getElementById("values").value
    var to_add = method+ ' '+field+' '+values+'\n';
    document.getElementById("transform-area").value += to_add
}

function update() {
    // AJAX method for updating the transformation preview.

    // Obtains and processes data from the transform box
    var transform_string = document.getElementById("transform-area").value
    var transform_list = transform_string.split(/\r?\n/);
    transform_list = transform_list.filter(e => e);

    // Start request
    var request = new XMLHttpRequest();

    // Form data to send
    var to_send = JSON.stringify({'transforms':transform_list});

    // When response is received
    request.onreadystatechange = function() {
        if (request.readyState == 4 && request.status == 200) {
            var response = request.responseText;
            document.getElementById("preview-area").value = response;
        }
    };

    // Request method and URL
    request.open("POST","update", true);

    // Send data
    request.send(to_send);
}